# Fedora Handheld

A remix of Fedora Linux, adapted to run on ARM-based handheld devices like the
PinePhone, PineTab or Librem 5.

There are already images based on Fedora that run on the PinePhone. This is yet
another attempt to do it, but with the official tooling used/endorsed by Fedora
for building OS images.

> **Beware: It's highly untested!**  
> If you've ever modded an Android phone the following might read familiar...

```
* I am not responsible for bricked devices, dead SD cards,
* thermonuclear war, or you getting fired because the alarm app failed. Please
* do some research if you have any concerns about features included in this ROM
* before flashing it! YOU are choosing to make these modifications, and if
* you point the finger at me for messing up your device, I will laugh at you.
```


## Current Status

There aren't any images yet that I could provide. I'm still collecting
information on how to build them properly.

The Fedora wiki lists these tools:

- https://weldr.io/lorax/
- https://imgfac.org/ (Official tool used by Fedora)
- https://pagure.io/appliance-tools (,,Easiest to use'' (TM))

There's also this tool:

- https://www.osbuild.org/documentation/ (,,The new hotness'')

But it currently isn't capable of building Fedora remixes.

The images are built using [kickstart](https://pagure.io/fedora-kickstarts).
I should take a look at `fedora-disk-*` kickstart files as examples.


### Building process for appliance-tools

If you're using appliance-tools, you'll want to look at the
fedora-arm-\* kickstart files as examples, but those are present only
in the f33 branch: https://pagure.io/fedora-kickstarts/tree/f33

I primarily work with appliance-tools, so for that one, the steps more
or less go like this:

Prep steps (only need to be done the first time):

``` bash
> sudo dnf install pykickstart mock
> sudo usermod -a -G mock $USER
> newgrp mock
```

Image creation steps (should be done each time):

``` bash
> git clone --branch f33 https://pagure.io/fedora-kickstarts
> ksflatten -v, --config fedora-arm-workstation.ks -o ./flat-f33-workstation.ks --version f33
> mock --root fedora-33-armhfp --install appliance-tools
> mock --root fedora-33-armhfp --copyin ./flat-f33-workstation.ks /
> mock --root fedora-33-armhfp --isolation=simple --chroot "/usr/bin/appliance-creator -c /flat-f33-workstation.ks -d -v --logfile appliance.log --cache /tmp/koji-appliance -o /app-output --format raw --name Fedora-Workstation-armhfp-33 --version 33 --release 1"
> mock --root fedora-33-armhfp --copyout /app-output/Fedora-Workstation-* .
```


## Long-term goal

For the future it'd be nice if this image gets promoted to an official Fedora
Spin. Maybe someday...


## TODO

Must replace the following packages (for legal reasons):

| Default package | Must replace with |
| :-------------: | :---------------: |
| fedora-release  | generic-release   |
| fedora-logos    | generic-logos     |
